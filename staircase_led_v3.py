
from machine import Pin, PWM, Timer
import utime

# blink internal light to show the boot is ok
internal_led = Pin(25, Pin.OUT)
internal_led.high()
utime.sleep(0.5)
internal_led.low()

stairsLights = []
stairsLightsPwm = []


#stairsLights.append(Pin(25, Pin.OUT))
stairsLights.append(Pin(0, Pin.OUT))
stairsLights.append(Pin(1, Pin.OUT))
stairsLights.append(Pin(2, Pin.OUT))
stairsLights.append(Pin(3, Pin.OUT))
stairsLights.append(Pin(4, Pin.OUT))
stairsLights.append(Pin(5, Pin.OUT))
stairsLights.append(Pin(8, Pin.OUT))
stairsLights.append(Pin(9, Pin.OUT))

lightsCount = len(stairsLights)

for light in stairsLights:
    pwm = PWM(light)
    pwm.freq(1000)
    stairsLightsPwm.append(pwm)
    

stairsLightsDuty = [0 for x in range(lightsCount)]
stairsLightsPwmDirection = [0 for x in range(lightsCount)]

# predefined times [ms]
timerPeriod = 100
tDelay = 800
tOn = 5000
tCurrent = 0
tFade = 512
tMax = (lightsCount-1) * tDelay + tOn + 1

# Flag to mark if walking upstairs (true) or downstairs (false)
walkingUpstairs = True


def allLedsOff():
    global stairsLightsPwm
    global tCurrent
    tCurrent = 0
    for led in stairsLightsPwm:
        led.duty_u16(0)
        
# initialize with all LED turned off
allLedsOff()
    
timUp = Timer()
timPwm = Timer()

motion_sensor_down = Pin(16, Pin.IN, Pin.PULL_UP)
motion_sensor_up = Pin(17, Pin.IN, Pin.PULL_UP)


def handleLedFade(timer):
    global stairsLightsPwm
    global stairsLights
    global stairsLightsPwmDirection
    global stairsLightsDuty
    global tFade
    for i, pwmDirection in enumerate(stairsLightsPwmDirection, start=0):
        if pwmDirection > 0:
            if stairsLightsDuty[i] < tFade:
                stairsLightsDuty[i] += 1
                stairsLightsPwm[i].duty_u16(int(stairsLightsDuty[i] * stairsLightsDuty[i]*256*256/(tFade*tFade)))
            else:
                stairsLightsPwmDirection[i] = 0
                stairsLights[i].value(1)
        if pwmDirection < 0:
            if stairsLightsDuty[i] >= 0:
                stairsLightsDuty[i] -= 1
                stairsLightsPwm[i].duty_u16(int(stairsLightsDuty[i] * stairsLightsDuty[i]*256*256/(tFade*tFade)))
            else:
                stairsLightsDuty[i] = 0
                stairsLightsPwmDirection[i] = 0
                stairsLights[i].value(0)
                
timPwm.init(freq=1000, mode=Timer.PERIODIC, callback=handleLedFade)

def handleLedOnTimer(timer):
    global stairsLights
    global tCurrent
    global timerPeriod
    global tDelay
    global tOn
    
    if tCurrent >= tMax:
        timer.deinit()
        allLedsOff()
        return
    # check which LED to turn on
    if tCurrent % tDelay == 0:
        ledNumber = int(tCurrent/tDelay)
        if ledNumber < lightsCount:
            if not walkingUpstairs:
                ledNumber = lightsCount - ledNumber - 1
            stairsLightsPwmDirection[ledNumber] = 1
            
    # check which LED to turn off
    if tCurrent >= tOn and (tCurrent - tOn) % tDelay == 0:
        ledNumber = int((tCurrent - tOn)/tDelay)
        if ledNumber < lightsCount:
            if not walkingUpstairs:
                ledNumber = lightsCount - ledNumber - 1
            stairsLightsPwmDirection[ledNumber] = -1
            
    tCurrent += timerPeriod
    

def iterateLedUpstairs(p):
    global timerPeriod
    global timUp
    global walkingUpstairs
    global tCurrent
    if tCurrent == 0:
        walkingUpstairs = True
        timUp.init(period=timerPeriod, mode=Timer.PERIODIC, callback=handleLedOnTimer)
    
def iterateLedDownstairs(p):
    global timerPeriod
    global timUp
    global walkingUpstairs
    global tCurrent
    if tCurrent == 0:
        walkingUpstairs = False
        timUp.init(period=timerPeriod, mode=Timer.PERIODIC, callback=handleLedOnTimer)

motion_sensor_down.irq(trigger=Pin.IRQ_FALLING, handler=iterateLedUpstairs)
motion_sensor_up.irq(trigger=Pin.IRQ_FALLING, handler=iterateLedDownstairs)
