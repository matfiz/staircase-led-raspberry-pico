# staircase-led-raspberry-pico

A set of small micropython scripts to operate LED mounted at the staircase. It's dedicated to Raspberry Pi Pico. 

## Versions
### Version 1 [Staircase LEDs v1](staircase_led_v1.py)
This script handles a basic functionality:
* support as many LED as you can connect to your Pico
* supports 2 motion sensors to trigger the LED cascade
* both the turn on delay between LEDs and the time each LED is on can be adjusted by variable located at the top of the script

### Version 2 [Staircase LEDs v2 - fading](staircase_led_v2.py)
It's the extension of v1. PWM is utilized to bring a nice fade in/out when the lights are turned on and off.

## Resources
* [MicroPython Docs](https://docs.micropython.org/en/latest/index.html)
* [Raspberry Pi Pico MicroPhython SDK](https://datasheets.raspberrypi.org/pico/raspberry-pi-pico-python-sdk.pdf)
* [Pico MicroPython examples](https://github.com/raspberrypi/pico-micropython-examples)

## Hardware
* Raspberry Pi Pico
* 3.3V step down (eg. [Pololu 2873](https://botland.com.pl/przetwornice-step-up-step-down/10676-s9v11f3s5c3-przetwornica-step-upstep-down-33v-15a-z-funkcja-odciecia-przy-niskim-napieciu-pololu-2873.html) )
* transistor module to run the LEDs (eg. [Iduino ST1168 with MOSFET IRF520](https://botland.com.pl/przelaczniki-cyfrowe/8236-modul-wykonawczy-mosfet-irf520-24v5a-iduino-st1168-5903351241212.html))
* staircase LEDs of your choice
* LED power supply, adjusted to your LEDs
* motion sensors (either a simple PIR or more sophisticated laser sensor like [F&F DRL-12](https://www.fif.com.pl/pl/czujniki-ruchu/1119-laserowy-czujnik-odleglosci-drl-12.html))
