from machine import Pin, PWM, Timer
import utime

stairsLights = []
stairsLightsPwm = []


stairsLights.append(Pin(25, Pin.OUT))
stairsLights.append(Pin(0, Pin.OUT))
stairsLights.append(Pin(1, Pin.OUT))

for light in stairsLights:
    pwm = PWM(light)
    pwm.freq(1000)
    stairsLightsPwm.append(pwm)
    

stairsLightsDuty = [0 for x in range(0,len(stairsLights))]
stairsLightsPwmDirection = [0 for x in range(0,len(stairsLights))]

# predefined times [ms]
timerPeriod = 100
tDelay = 500
tOn = 2000
tCurrent = 0
tFade = 256
tMax = (len(stairsLights)-1) * tDelay + tOn

# Flag to mark if walking upstairs (true) or downstairs (false)
walkingUpstairs = True


def allLedsOff():
    global stairsLights
    for led in stairsLights:
        led.value(0)
        
# initialize with all LED turned off
allLedsOff()
    
timUp = Timer()
timPwm = Timer()

motion_sensor_down = Pin(16, Pin.IN, Pin.PULL_UP)
motion_sensor_up = Pin(17, Pin.IN, Pin.PULL_UP)


def handleLedFade(timer):
    global stairsLightsPwm
    global stairsLights
    global stairsLightsPwmDirection
    global stairsLightsDuty
    global tFade
    for i, pwmDirection in enumerate(stairsLightsPwmDirection, start=0):
        if pwmDirection > 0:
            if stairsLightsDuty[i] < tFade:
                stairsLightsDuty[i] += 1
                stairsLightsPwm[i].duty_u16(stairsLightsDuty[i] * stairsLightsDuty[i])
            else:
                stairsLightsPwmDirection[i] = 0
                stairsLights[i].value(1)
        if pwmDirection < 0:
            if stairsLightsDuty[i] >= 0:
                stairsLightsDuty[i] -= 1
                stairsLightsPwm[i].duty_u16(stairsLightsDuty[i] * stairsLightsDuty[i])
            else:
                stairsLightsDuty[i] = 0
                stairsLightsPwmDirection[i] = 0
                stairsLights[i].value(0)
                
timPwm.init(freq=1000, mode=Timer.PERIODIC, callback=handleLedFade)

def handleLedOnTimer(timer):
    global stairsLights
    global tCurrent
    global timerPeriod
    global tDelay
    global tOn
    tCurrent += timerPeriod
    if tCurrent >= tMax:
        tCurrent = 0
        timer.deinit()
        allLedsOff()
    # check which LED to turn on
    if tCurrent % tDelay == 0:
        ledNumber = int(tCurrent/tDelay)-1
        if not walkingUpstairs:
            ledNumber = len(stairsLights) - ledNumber - 1
        if ledNumber < len(stairsLights):
            stairsLightsPwmDirection[ledNumber] = 1
            
    # check which LED to turn off
    for ledNumber in range(0,len(stairsLights)):   
        if tCurrent % (ledNumber*tDelay+tOn) == 0:
            if ledNumber < len(stairsLights):
                if not walkingUpstairs:
                    ledNumber = len(stairsLights) - ledNumber - 1
                stairsLightsPwmDirection[ledNumber] = -1
    

def iterateLedUpstairs(p):
    global timerPeriod
    global timUp
    global walkingUpstairs
    walkingUpstairs = True
    timUp.init(period=timerPeriod, mode=Timer.PERIODIC, callback=handleLedOnTimer)
    
def iterateLedDownstairs(p):
    global timerPeriod
    global timUp
    global walkingUpstairs
    walkingUpstairs = False
    timUp.init(period=timerPeriod, mode=Timer.PERIODIC, callback=handleLedOnTimer)

motion_sensor_down.irq(trigger=Pin.IRQ_FALLING, handler=iterateLedUpstairs)
motion_sensor_up.irq(trigger=Pin.IRQ_FALLING, handler=iterateLedDownstairs)


