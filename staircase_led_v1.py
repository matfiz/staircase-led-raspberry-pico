from machine import Pin, Timer
import utime

stairsLights = []

# Define your LEDs below
stairsLights.append(Pin(25, Pin.OUT)) #internal LED, added for testing
stairsLights.append(Pin(0, Pin.OUT))
stairsLights.append(Pin(1, Pin.OUT))

# predefined times [ms]
timerPeriod = 100
tDelay = 500
tOn = 2000
tCurrent = 0
tMax = (len(stairsLights)-1) * tDelay + tOn

# Flag to mark if walking upstairs (true) or downstairs (false)
walkingUpstairs = True


def allLedsOff():
    global stairsLights
    for led in stairsLights:
        led.value(0)
        
# initialize with all LED turned off
allLedsOff()
    
timUp = Timer()

motion_sensor_down = Pin(16, Pin.IN, Pin.PULL_UP)
motion_sensor_up = Pin(17, Pin.IN, Pin.PULL_UP)


def handleLedOnTimer(timer):
    global stairsLights
    global tCurrent
    global timerPeriod
    global tDelay
    global tOn
    global walkingUpstairs
    tCurrent += timerPeriod
    if tCurrent >= tMax:
        tCurrent = 0
        timer.deinit()
        allLedsOff()
    # check which LED to turn on
    if tCurrent % tDelay == 0:
        ledNumber = int(tCurrent/tDelay)-1
        if not walkingUpstairs:
            ledNumber = len(stairsLights) - ledNumber - 1
        if ledNumber < len(stairsLights):
            stairsLights[ledNumber].value(1)
            
    # check which LED to turn off
    for ledNumber in range(0,len(stairsLights)):
        if tCurrent % (ledNumber*tDelay+tOn) == 0:
            if ledNumber < len(stairsLights):
                if not walkingUpstairs:
                    ledNumber = len(stairsLights) - ledNumber - 1
                stairsLights[ledNumber].value(0)
    

def iterateLedUpstairs(p):
    global timerPeriod
    global timUp
    global walkingUpstairs
    walkingUpstairs = True
    timUp.init(period=timerPeriod, mode=Timer.PERIODIC, callback=handleLedOnTimer)
    
def iterateLedDownstairs(p):
    global timerPeriod
    global timUp
    global walkingUpstairs
    walkingUpstairs = False
    timUp.init(period=timerPeriod, mode=Timer.PERIODIC, callback=handleLedOnTimer)


motion_sensor_down.irq(trigger=Pin.IRQ_FALLING, handler=iterateLedUpstairs)
motion_sensor_up.irq(trigger=Pin.IRQ_FALLING, handler=iterateLedDownstairs)


